# Ancile Microsoft Customer Experience Improvement Program Plugin
### About
https://gitlab.com/misc-scripts1/windows/ancile/AncilePlugin_MSCEIP

Microsoft Customer Experience Improvement Program Plugin disables the MSCEIP tasks.

This is a plugin that requires Ancile_Core (https://gitlab.com/misc-scripts1/windows/ancile/Ancile_Core) 

For more information go to https://voat.co/v/ancile